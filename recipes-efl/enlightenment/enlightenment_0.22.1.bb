DESCRIPTION = "The Enlightenment Window Manager Version 0.22.1"

require ${BPN}.inc

S = "${WORKDIR}/git"
B = "${S}"

SRC_URI = "\
    git://git.enlightenment.org/core/enlightenment.git;protocol=https;tag=v${PV};nobranch=1 \
    file://enlightenment_start.oe \
    file://applications.menu \
"

