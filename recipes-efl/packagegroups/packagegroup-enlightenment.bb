SUMMARY = "The software for Enlightenment"
DESCRIPTION = "A set of packages belong to Enlightenment"

LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
    packagegroup-enlightenment \
    "

ALLOW_EMPTY_${PN} = "1"

RDEPENDS_${PN} += "\
    "

RDEPENDS_${PN} += " enlightenment enlightenment-session \
                    enlightenment-config-default \
                    enlightenment-config-mobile \
                    enlightenment-config-standard \
                    enlightenment-config-tiling \
                    enlightenment-backgrounds \
		    ttf-dejavu-sans \
		    ttf-dejavu-serif \
		    ttf-liberation-mono \
		    ttf-liberation-sans \
		    ttf-liberation-serif \
                  "

