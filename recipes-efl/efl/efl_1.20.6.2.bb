require ${BPN}_1.20.inc

SRCREV="411b6345c80e50773702e210204e906b0dd8f672"

SRC_URI = "\
   git://git.enlightenment.org/core/efl.git;protocol=https;branch=efl-1.20 \
"

S = "${WORKDIR}/git"
B = "${S}"

CFLAGS_prepend = "-fPIC "

