require ${BPN}_1.20.inc

SRCREV="d1e5e3ff5ee19e44e722e153ffd260f4a1f04bb2"
SRC_URI = "\
   git://git.enlightenment.org/core/efl.git;protocol=https;branch=efl-1.20 \
   file://fix-configure-wayland-protocols-path.patch \
"

S = "${WORKDIR}/git"
B = "${S}"

CFLAGS_prepend = "-fPIC "

# DEFAULT_PREFERENCE = "-1"
